from django.conf import settings
from django_elasticsearch_dsl import Document, Index, fields
from elasticsearch_dsl import (
    analyzer,
    InnerDoc,
    Keyword,
    Nested,
    Text,
    Integer,
    Float,
)

from back.models import Article

INDEX = Index("article")
INDEX.settings(
    number_of_shards=1,
    number_of_replicas=1
)

__all__ = (
    'ArticleDoc'
)

html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter=["lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)


@INDEX.doc_type
class ArticleDoc(Document):
    title = fields.TextField(
        analyzer=html_strip,
        fields={
            'raw': fields.TextField(analyzer='keyword'),
        }
    )
    author = fields.TextField()
    text = fields.TextField()
    category = fields.TextField()

    class Django:
        model = Article


ArticleDoc.init()
