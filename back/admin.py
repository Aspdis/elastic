from django.contrib import admin

from .models import *


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    """Book admin."""

    list_display = ('title', 'text', 'author', 'category')
    search_fields = ('title',)
