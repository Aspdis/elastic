from django.db import models


class Article(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True)
    title = models.CharField(
        max_length=256, default="", blank=True, verbose_name="Название"
    )
    text = models.TextField(
        default="", blank=True, verbose_name="Содержание"
    )
    author = models.CharField(
        max_length=256, default="", blank=True, verbose_name="Автор"
    )
    category = models.CharField(
        max_length=256, default="", blank=True, verbose_name="Категория"
    )

    def __str__(self):
        return self.title
