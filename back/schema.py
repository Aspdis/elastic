from graphene_elastic import ElasticsearchObjectType
from search_indexes.documents.article import ArticleDoc
from graphene import Node
from graphene_elastic.filter_backends import (
    FilteringFilterBackend,
    SearchFilterBackend,
    HighlightFilterBackend,
    OrderingFilterBackend,
    SimpleQueryStringBackend
)


class Article(ElasticsearchObjectType):
    class Meta(object):
        document = ArticleDoc
        interfaces = (Node,)
        filter_backends = [
            FilteringFilterBackend,
            SearchFilterBackend,
            OrderingFilterBackend,
            HighlightFilterBackend,
            SimpleQueryStringBackend
        ]
        filter_fields = {
            'title': 'title.raw'
        }
        simple_query_string_options = {
            "fields": ["title", "text", "author"],
            "boost": 2,
        }
        search_fields = {
            'title': {'boost': 4},
            'text': {'boost': 2},
            'author': None,
        }
        ordering_fields = {
            'title': 'title.raw',
            'text': 'text.raw',
            'author': 'author.raw',
        }
        highlight_fields = {
            'title': {
                'enabled': True,
                'options': {
                    'pre_tags': ["<b>"],
                    'post_tags': ["</b>"],
                }
            },
            'text': {
                'options': {
                    'fragment_size': 50,
                    'number_of_fragments': 3
                }
            },
            'author': {
                'enabled': True,
                'options': {
                    'pre_tags': ["<b>"],
                    'post_tags': ["</b>"],
                }
            },
        }
