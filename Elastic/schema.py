import graphene
from back.schema import Article
from graphene_elastic import ElasticsearchConnectionField


class Query(graphene.ObjectType):
    """
    запросы на чтение

    """

    article = ElasticsearchConnectionField(Article)


class Mutation(graphene.ObjectType):
    """
    запросы-мутаторы

    """

    pass


schema = graphene.Schema(query=Query)
